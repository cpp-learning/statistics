#include "stdafx.h"
#include "std_includes.h"

#include "Statistics.h"

template <class T> class Statistics;
int main()
{

  Statistics<int> intStats;
  vector<int> numbers;
  numbers.push_back(1);
  numbers.push_back(3);
  numbers.push_back(3);
  numbers.push_back(-1);
  numbers.push_back(22);
  numbers.push_back(31);
  numbers.push_back(2);
  numbers.push_back(-1);
  numbers.push_back(1);
  numbers.push_back(5);
  numbers.push_back(7);

  vector<int> numbetsMode = intStats.get_mode(numbers);

  cout << "\nMEAN: " << intStats.get_mean(numbers) << endl;
  cout << "\nMODE: got " << numbetsMode.size() << " result\n";
  for (vector<int>::iterator it = numbetsMode.begin(); it != numbetsMode.end(); ++it) {
    cout << "-\t" << (*it) << endl;
  }

  keepOpen();
  return 0;
}
