#include "Statistics.h"
#include "stdafx.h"
#include "Statistics.h"
#include <algorithm>

template<class T>
Statistics<T>::Statistics()
{
}

template<class T>
Statistics<T>::~Statistics()
{
}

template<class T>
vector<T> Statistics<T>::get_mode(vector<T> values)
{
  const map<T, unsigned> occurences = this->count_occurences(values);
  return this->get_most_common(occurences);
}

template<class T>
map<T, unsigned> Statistics<T>::count_occurences(vector<T> values)
{
  map<T, unsigned> occurences;
  typename vector<T>::iterator it = values.begin();
  for (; it != values.end(); ++it) {
    const T value = (*it);
    typename map<T, unsigned>::iterator occurence = occurences.find(value);
    if (occurence != occurences.end()) {
      ++occurence->second;
    } else {
      occurences[value] = 1;
    }
  }

  return occurences;
}

template<class T>
vector<T> Statistics<T>::get_most_common(map<T, unsigned> occurences)
{
  vector<T> mostCommon;
  unsigned count = 0;

  typename map<T, unsigned>::iterator it = occurences.begin();
  for (; it != occurences.end(); ++it) {
    if (it->second > count) {
      mostCommon.clear();
      mostCommon.push_back(it->first);
      count = it->second;
    }
    else if (it->second == count) {
      mostCommon.push_back(it->first);
    }
  }

  return mostCommon;
}


template<class T>
T Statistics<T>::get_mean(vector<T> values)
{
  T sum = this->sum(values);
  unsigned size = values.size();
  return sum / size;
}

template<class T>
T Statistics<T>::sum(vector<T> values)
{
  typename vector<T>::iterator it = values.begin();
  T sum = (*it);
  ++it;
  for (; it != values.end(); ++it) {
    sum += (*it);
  }
  return sum;
}

/* TODO sort copy of data, DO NOT modify input */
template<class T>
T Statistics<T>::get_median(vector<T> values)
{
  sort(values.begin(), values.end());
  if (values.size() % 2) {
    size_t middle_position = values.size() / 2;
    return values.at(middle_position);
  }
  else {
    size_t to = values.size() / 2;
    size_t from = to -1;
    vector<T> middle_values(&values[from], &values[to]);
    return this->get_mean(middle_values);
  }
  return T();
}