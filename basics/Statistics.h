#pragma once
#include <vector>
#include <map>

using namespace std;

template <class T> class Statistics
{
private:
  map<T, unsigned> count_occurences(vector<T> values);
  vector<T> get_most_common(map<T, unsigned> occurences);
  T sum(vector<T> values);
public:
  Statistics();
  ~Statistics();

 /* TODO create versions with compare argument */
 /* TODO create versions with reference arguments
 */
  vector<T> get_mode(vector<T> values);
  T get_mean(vector<T> values);
  T get_median(vector<T> values);
};

#include "Statistics.tpp"
